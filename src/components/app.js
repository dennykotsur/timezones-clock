import React, { Component } from 'react';
import ClockSelector from '../containers/clock_selector';
import ClockList from '../containers/clock_list';

export default class App extends Component {
    
  render() {
    return (
      <div>
          <ClockSelector />
          <ClockList />
      </div>
    );
  }
}
