import { combineReducers } from 'redux';
import ClockReducer from './recucers_clock';

const rootReducer = combineReducers({
  selectedTimezones: ClockReducer,
});

export default rootReducer;
