import { ADD_CLOCK } from '../actions/types';

export default function(state = {}, action) {
    switch (action.type) {
        case ADD_CLOCK:
            return { ...state, [action.payload]: action.payload };
    }
    return state
}