import { ADD_CLOCK } from './types';


export function addClock(tz) {
    return {
        type: ADD_CLOCK,
        payload: tz
    }
}