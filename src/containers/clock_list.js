import _ from 'lodash';
import { connect } from 'react-redux';

import React, { Component } from 'react';
import Moment from 'react-moment';
import moment from 'moment';
import 'moment-timezone';
import AnalogClock, { Themes } from 'react-analog-clock';


class ClockList extends Component {
    renderList() {
        const currentTime = moment();
        return _.map(this.props.selectedTimezones, timezone => {
            const offset = (moment().tz(timezone).utcOffset() / 60).toString();
            return (
                <div className="col-md-5 clock-item" key={timezone}>
                    <AnalogClock theme={Themes.navy} gmtOffset={offset}/>
                    <div className="clock-title">
                        <b> {timezone}: </b>
                        <Moment
                            tz={timezone}
                            format="HH:mm:ss"
                            interval={1000}
                        />
                        <span className="timezone-value">
                            {currentTime.tz(timezone).format("Z z")}
                        </span>
                    </div>
                </div>
            )
        });
    }
    
    render() {
        return (
            <div className="row">
                {this.renderList()}
            </div>
        )
    }
}


function mapStateToProps({ selectedTimezones }) {
    return { selectedTimezones }
}

export default connect(mapStateToProps)(ClockList)