import React, { Component } from 'react';
import moment from 'moment';
import 'moment-timezone';
import { connect } from 'react-redux';
import { addClock } from '../actions/index';


class ClockSelector extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentTimezone: 'Africa/Abidjan'
        };
        
        this.selectTimezone = this.selectTimezone.bind(this);
        this.addClock = this.addClock.bind(this);
    }
    
    renderTimezones() {
        return moment.tz.names().map(name => {
           return (
               <option key={name}>
                   {name}
               </option>
           )
        });
    }
    
    selectTimezone(e) {
        this.setState({
            currentTimezone: e.target.value
        })
    }
    
    addClock() {
        this.props.addClock(this.state.currentTimezone);
    }
    
    render() {
        return (
            <div className="row select-timezone">
               <div className="form-group col-md-10">
                   <select className="form-control" onChange={this.selectTimezone}>
                       {this.renderTimezones()}
                   </select>
               </div>
               <button
                   className="col-md-2 btn btn-primary"
                   onClick={this.addClock}
               >
                   Add Clock
               </button>
            </div>
        )
    }
}

export default connect(null, { addClock })(ClockSelector)


